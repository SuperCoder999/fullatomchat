import dotenv from "dotenv";
dotenv.config();

export const IS_DEV: boolean = /^dev(elopment)?$/.test(process.env.NODE_ENV || "dev");
export const MONGO_URI = `mongodb+srv://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@atomchat.cg9md.mongodb.net/${process.env.MONGO_DBNAME}?retryWrites=true&w=majority`;
