import express, { Request, Response, NextFunction } from "express";
import api from "./api";
import mongoose from "mongoose";
import session from "express-session";
import createMongoStore from "connect-mongo";
import passport from "passport";
import { Strategy as LocalStrategy } from "passport-local";
import { IFullUser } from "./api/user/user.types";
import { RGetUserById, RGetUserByEmail } from "./api/user/user.repository";
import { oicast } from "./api/utils";
import { compareSync } from "bcryptjs";
import { MONGO_URI, IS_DEV } from "./config";
import path from "path";

const MongoStore = createMongoStore(session);

const server: express.Express = express();

mongoose.connect(MONGO_URI, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true
});

mongoose.set("debug", IS_DEV);

mongoose.connection.on("error", (err: Error): void => {
    console.log("Mongo error:", err);
    process.exit(1);
});

server.use(express.json());
server.use(express.urlencoded({ extended: true }));

server.use(
    session({
        name: "session",
        resave: true,
        saveUninitialized: true,
        secret: "SecretKey153676878",
        cookie: {
            maxAge: 24 * 60 * 60 * 1000,
            sameSite: false
        },
        store: new MongoStore({
            mongooseConnection: mongoose.connection,
            collection: "session",
            stringify: false
        })
    })
);

server.use(passport.initialize());
server.use(passport.session());

server.use((req: Request, res: Response, next: NextFunction): void => {
    res.setHeader("Access-Control-Allow-Origin", ["http://localhost:3000"]);
    res.setHeader("Access-Control-Allow-Methods", ["GET", "POST", "PUT", "DELETE"]);
    res.setHeader("Access-Control-Allow-Headers", ["content-type"]);
    res.setHeader("Access-Control-Allow-Credentials", "true");
    next();
});

passport.serializeUser((user: IFullUser, next): void => {
    next(null, { _id: user._id });
});

passport.deserializeUser(async (user: { _id: string }, next): Promise<void> => {
    const resultUser: IFullUser = await RGetUserById(oicast(user._id));
    next(null, resultUser);
});

passport.use(
    "local",
    new LocalStrategy(
        {
            passReqToCallback: true,
            passwordField: "password",
            usernameField: "email"
        },
        async (req: Request, email: string, password: string, next): Promise<void> => {
            try {
                const user: IFullUser = await RGetUserByEmail(email);

                if (!compareSync(password, user.password)) {
                    return next({ message: "Username or password is not correct" }, null);
                }

                return next(null, user);
            } catch (err) {
                return next({ message: "Username or password is not correct" }, null);
            }
        }
    )
);

server.use("/api", api);

if (!IS_DEV) {
    server.use(express.static(path.join(__dirname, "../../", "client", "build")));

    server.get("/*", (req: Request, res: Response) => {
        res.sendFile(path.join(__dirname, "../../", "client", "build", "index.html"));
    });
}

server.use((err: Error | { status?: number, message: string }, req: Request, res: Response, next: NextFunction) => {
    let e: { message: string } = { message: "" };
    let status: number = 500;

    if (!err.hasOwnProperty("status")) {
        e.message = err.message;
        status = 400
    } else {
        err = (err as { status: number, message: string });
        e.message = err.message;
        status = err.status || 500;
    }

    res.status(status).json(e);
});

const port: number = Number(process.env.PORT) || 3001;

server.listen(port, "", () => {
    console.log(`Server is listening on port ${port}`);
});

export default server;
