import { Joi, Segments } from "celebrate";

export const loginValidator = {
    [Segments.BODY]: Joi.object().keys({
        email: Joi.string().email().trim(true).required(),
        password: Joi.string().trim(true).required()
    })
};

export const addUserValidator = {
    [Segments.BODY]: Joi.object().keys({
        isAdmin: Joi.boolean().default(false),
        email: Joi.string().email().trim(true).required(),
        password: Joi.string().trim(true).min(6).required(),
        firstName: Joi.string().trim(true).required(),
        lastName: Joi.string().trim(true).allow("")
    })
};

export const updateUserValidator = {
    [Segments.BODY]: Joi.object().keys({
        isAdmin: Joi.boolean(),
        email: Joi.string().email().trim(true),
        firstName: Joi.string().trim(true),
        lastName: Joi.string().trim(true).allow("")
    })
};

export const IDInParamsValidator = {
    [Segments.PARAMS]: Joi.object().keys({
        id: Joi.string().trim(true).regex(/^[a-z0-9]{24}$/i).required()
    })
}
