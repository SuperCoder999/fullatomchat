import User from "./user.model";
import { IFullUser, IUser, IPartialUser } from "./user.types";
import { Types } from "mongoose";
import { I_IDResult, I_OKResult } from "../types";
import { IFullMessage } from "../message/message.types";
import { RGetAllMessages } from "../message/message.repository";
import Message from "../message/message.model";

export async function RAddUser(data: IUser): Promise<I_IDResult> {
    const checkingUser = await RGetUserByEmail(data.email, false);

    if (checkingUser) {
        throw {
            status: 400,
            message: "This email is already taken"
        }
    }

    const { _id } = await User.create(data);
    return { _id };
}

export async function RUpdateUser(id: Types.ObjectId, data: IPartialUser): Promise<IFullUser> {
    if (data.email) {
        const checkingUser = await RGetUserByEmail(data.email, false);

        if (checkingUser && (checkingUser ? !checkingUser._id.equals(id) : false)) {
            throw {
                status: 400,
                message: "This email is already taken"
            }
        }
    }

    const result: IFullUser & { n?: number } = await User.updateOne({ _id: id }, data).lean().exec() as IFullUser;

    if (result.n === 0) {
        throw {
            status: 404,
            message: "User not found"
        };
    }

    return result;
}

export async function RGetAllUsers(): Promise<IFullUser[]> {
    const result: IFullUser[] = await User.find({}).lean().exec() as IFullUser[];
    return result;
}

export async function RGetUserById(id: Types.ObjectId): Promise<IFullUser> {
    const result: IFullUser | null = await User.findOne({ _id: id }).lean().exec() as IFullUser;

    if (!result) {
        throw {
            status: 404,
            message: "User not found"
        }
    }

    return result;
}

export async function RGetUserByEmail(email: string, throwError: boolean = true): Promise<IFullUser> {
    const result: IFullUser | null = await User.findOne({ email }).lean().exec() as IFullUser;

    if (!result && throwError) {
        throw {
            status: 404,
            message: "User not found"
        }
    }

    return result;
}

export async function RDeleteUser(id: Types.ObjectId): Promise<I_OKResult> {
    const messages: IFullMessage[] = await RGetAllMessages();
    const userMessages = messages.filter((message: IFullMessage): boolean => message.author._id.equals(id));

    await Promise.all(
        userMessages.map(async (message: IFullMessage): Promise<void> => {
            await Message.deleteOne({ _id: message._id.toHexString() }).lean().exec()
        })
    );

    const result: I_OKResult & { deletedCount?: number } | null = await User.deleteOne({ _id: id }).lean().exec();
    
    if (!result || result.deletedCount === 0) {
        throw {
            status: 404,
            message: "User not found"
        }
    }

    return (result as I_OKResult);
}
