import {
    RAddUser,
    RDeleteUser,
    RGetAllUsers,
    RGetUserById,
    RUpdateUser
} from "./user.repository";

import { Types } from "mongoose";
import { IUser, IFullUser, IPartialUser } from "./user.types";
import { I_OKResult, I_IDResult, IError } from "../types";

export async function SAddUser(data: IUser): Promise<I_IDResult | IError> {
    try {
        const result: I_IDResult = await RAddUser(data);
        return result;
    } catch (err) {
        return err;
    }
}

export async function SUpdateUser(id: Types.ObjectId, data: IPartialUser): Promise<IFullUser | IError> {
    try {
        const result: IFullUser = await RUpdateUser(id, data);
        return result;
    } catch (err) {
        return err;
    }
}

export async function SGetAllUsers(): Promise<IFullUser[]> {
    const result: IFullUser[] = await RGetAllUsers();
    return result;
}

export async function SGetUserById(id: Types.ObjectId): Promise<IFullUser | IError> {
    try {
        const result: IFullUser = await RGetUserById(id);
        return result;
    } catch (err) {
        return err;
    }
}

export async function SDeleteUser(id: Types.ObjectId): Promise<I_OKResult | IError> {
    try {
        const result: I_OKResult = await RDeleteUser(id);
        return result;
    } catch (err) {
        return err;
    }
}
