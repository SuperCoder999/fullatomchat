import { model, Schema } from "mongoose";
import { hashSync } from "bcryptjs";

const User = new Schema(
    {
        email: {
            type: String,
            unique: true,
            index: true,
            trim: true,
            required: true
        },
        password: {
            type: String,
            trim: true
        },
        firstName: {
            type: String,
            trim: true,
            required: true
        },
        lastName: {
            type: String,
            trim: true
        },
        isAdmin: {
            type: Boolean,
            default: false
        }
    },
    {
        collection: "users",
        timestamps: true
    }
);

User.pre("save", async function (next): Promise<void> {
    if (this.isNew || this.isModified("password")) {
        this.set("password", hashSync(this.get("password"), 8)); 
    }

    next();
});

export default model("User", User);
