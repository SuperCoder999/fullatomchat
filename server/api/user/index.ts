import { Router } from "express";

import {
    addUser,
    getAllUsers,
    getUserById,
    deleteUser,
    updateUser,
    loginUser,
    getProfile,
    logoutUser
} from "./user.controller";

import {
    updateUserValidator,
    addUserValidator,
    loginValidator,
    IDInParamsValidator
} from "./user.validators";

import { respond, isAuthorized, isAdmin } from "../middleware";
import validate from "../validate";

const router: Router = Router();

router
    .get(
        "/",
        isAuthorized,
        isAdmin,
        getAllUsers,
        respond
    )
    .get(
        "/profile",
        isAuthorized,
        getProfile,
        respond
    )
    .get(
        "/:id",
        isAuthorized,
        isAdmin,
        validate(IDInParamsValidator),
        getUserById,
        respond
    )
    .post(
        "/login",
        validate(loginValidator),
        loginUser,
        respond
    )
    .post(
        "/logout",
        logoutUser,
        respond
    )
    .post(
        "/",
        isAuthorized,
        isAdmin,
        validate(addUserValidator),
        addUser,
        respond
    )
    .put(
        "/:id",
        isAuthorized,
        isAdmin,
        validate(updateUserValidator),
        validate(IDInParamsValidator),
        updateUser,
        respond
    )
    .delete(
        "/:id",
        isAuthorized,
        isAdmin,
        validate(IDInParamsValidator),
        deleteUser,
        respond
    );

export default router;
