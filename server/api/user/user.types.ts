import { Types } from "mongoose";

export interface IUser {
    password: string;
    email: string;
    firstName: string;
    lastName?: string;
}

export interface IPartialUser {
    email?: string;
    password?: string;
    firstName?: string;
    lastName?: string;
}

export interface IFullUser extends IUser {
    _id: Types.ObjectId;
}
