import {
    SGetUserById,
    SAddUser,
    SDeleteUser,
    SGetAllUsers,
    SUpdateUser
} from "./user.service";

import { Request, Response, NextFunction } from "express";
import { I_IDResult, I_OKResult, IError } from "../types";
import { IFullUser } from "./user.types";
import { oicast } from "../utils";
import passport from "passport";

export async function addUser(req: Request, res: Response, next: NextFunction) {
    const result: I_IDResult | IError = await SAddUser(req.body);
    res.data = result;
    next();
}

export async function updateUser(req: Request, res: Response, next: NextFunction) {
    const result: IFullUser | IError = await SUpdateUser(oicast(req.params.id), req.body);
    res.data = result;
    next();
}

export async function getAllUsers(req: Request, res: Response, next: NextFunction) {
    const result: IFullUser[] = await SGetAllUsers();
    res.data = result;
    next();
}

export async function getUserById(req: Request, res: Response, next: NextFunction) {
    const result: IFullUser | IError = await SGetUserById(oicast(req.params.id));
    res.data = result;
    next();
}

export async function deleteUser(req: Request, res: Response, next: NextFunction) {
    const result: I_OKResult | IError = await SDeleteUser(oicast(req.params.id));
    res.data = result;
    next();
}

export function getProfile(req: Request, res: Response, next: NextFunction) {
    res.data = req.user;
    next();
}

export function loginUser(req: Request, res: Response, next: NextFunction) {
    passport.authenticate("local", (err, user) => {
        if (err) return next(err);

        req.logIn(user, err => {
            if (err) return next(err);
            res.json({ isAdmin: user.isAdmin });
        });
    })(req, res, next);
}

export function logoutUser(req: Request, res: Response, next: NextFunction) {
    req.logOut();
    next();
}
