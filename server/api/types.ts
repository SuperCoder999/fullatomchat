export interface I_OKResult {
    ok?: number;
}

export interface I_IDResult {
    _id: string;
}

export interface IError {
    status: number;
    message: string;
}
