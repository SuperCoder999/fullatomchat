import { Router } from "express";
import users from "./user";
import messages from "./message";

const router: Router = Router();

router.use("/users", users);
router.use("/messages", messages);

export default router;
