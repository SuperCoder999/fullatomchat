import { celebrate, SchemaOptions } from "celebrate"
import { RequestHandler } from "express"

export default (schema: SchemaOptions): RequestHandler => {
    return celebrate(schema, {
        allowUnknown: false,
        abortEarly: false,
        stripUnknown: {
            objects: true
        }
    })
};
