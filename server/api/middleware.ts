import { Request, Response, NextFunction } from "express";

export function respond(req: Request, res: Response, next: NextFunction) {
    if (res.data) {
        if (res.data.message) {
            next(res.data);
        } else {
            res.json(res.data);
        }
    } else {
        res.json({ success: true });
    }
}

export function isAdmin(req: Request, res: Response, next: NextFunction) {
    if (req.user.isAdmin) {
        return next();
    }

    next({
        status: 403,
        message: "Not admin"
    });
}

export function isAuthorized(req: Request, res: Response, next: NextFunction) {
    if (req.user) {
        return next();
    }

    next({
        status: 401,
        message: "Not authorized"
    })
}
