import { Schema, model } from "mongoose";

const message = new Schema(
    {
        text: {
            type: String,
            trim: true,
            required: true
        },
        author: {
            type: Schema.Types.ObjectId,
            ref: "User",
            required: true
        },
        likeCount: {
            type: Number,
            default: 0
        },
        likers: {
            type: [Schema.Types.ObjectId],
            default: []
        },
        dislikeCount: {
            type: Number,
            default: 0
        },
        dislikers: {
            type: [Schema.Types.ObjectId],
            default: []
        },
        editedAt: {
            type: Date,
            default: null
        }
    },
    {
        collection: "messages",
        timestamps: true
    }
);

export default model("Message", message);
