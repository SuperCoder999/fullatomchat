import { Segments, Joi } from "celebrate";

export const addMessageValidator = {
    [Segments.BODY]: {
        text: Joi.string().trim(true).required()
    }
}

export const updateMessageValidator = {
    [Segments.BODY]: {
        text: Joi.string().trim(true)
    }
}

export const reactValidator = {
    [Segments.BODY]: {
        isLike: Joi.boolean().required()
    }
}

export const IDInParamsValidator = {
    [Segments.PARAMS]: {
        id: Joi.string().trim(true).regex(/^[0-9a-z]{24}$/i).required()
    }
}
