import { Router } from "express";

import {
    addMessage,
    deleteMessage,
    getAllMessages,
    getMessageById,
    updateMessage,
    reactMessage
} from "./message.controller";

import {
    addMessageValidator,
    updateMessageValidator,
    IDInParamsValidator,
    reactValidator
} from "./message.validators";

import { isAuthorized, respond } from "../middleware";
import validate from "../validate";

const router: Router = Router();

router
    .get(
        "/",
        isAuthorized,
        getAllMessages,
        respond
    )
    .get(
        "/:id",
        isAuthorized,
        validate(IDInParamsValidator),
        getMessageById,
        respond
    )
    .post(
        "/",
        isAuthorized,
        validate(addMessageValidator),
        addMessage,
        respond
    )
    .put(
        "/react/:id",
        isAuthorized,
        validate(reactValidator),
        validate(IDInParamsValidator),
        reactMessage,
        respond
    )
    .put(
        "/:id",
        isAuthorized,
        validate(updateMessageValidator),
        validate(IDInParamsValidator),
        updateMessage,
        respond
    )
    .delete(
        "/:id",
        isAuthorized,
        validate(IDInParamsValidator),
        deleteMessage,
        respond
    );

export default router;
