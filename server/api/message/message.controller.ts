import {
    SAddMessage,
    SDeleteMessage,
    SGetAllMessages,
    SGetMessageById,
    SUpdateMessage,
    SReactMessage
} from "./message.service";

import { Request, Response, NextFunction } from "express";
import { I_IDResult, IError, I_OKResult } from "../types";
import { IFullMessage } from "./message.types";
import { oicast } from "../utils";

export async function addMessage(req: Request, res: Response, next: NextFunction): Promise<void> {
    const result: I_IDResult | IError = await SAddMessage({ text: req.body.text, author: req.user._id });
    res.data = result;
    next();
}

export async function updateMessage(req: Request, res: Response, next: NextFunction): Promise<void> {
    const result: IFullMessage | IError = await SUpdateMessage(oicast(req.params.id), req.body);
    res.data = result;
    next();
}

export async function getAllMessages(req: Request, res: Response, next: NextFunction): Promise<void> {
    const result: IError | IFullMessage[] = await SGetAllMessages();
    res.data = result;
    next();
}

export async function getMessageById(req: Request, res: Response, next: NextFunction): Promise<void> {
    const result: IError | IFullMessage = await SGetMessageById(oicast(req.params.id));
    res.data = result;
    next();
}

export async function deleteMessage(req: Request, res: Response, next: NextFunction): Promise<void> {
    const result: I_OKResult | IError = await SDeleteMessage(oicast(req.params.id));
    res.data = result;
    next();
}

export async function reactMessage(req: Request, res: Response, next: NextFunction): Promise<void> {
    const result: IFullMessage | IError = await SReactMessage(oicast(req.params.id), req.user._id, req.body.isLike);
    res.data = result;
    next();
}
