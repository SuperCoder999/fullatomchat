import { Types } from "mongoose";

interface IBaseMessage {
    text: string;
}

export interface IMessage extends IBaseMessage {
    author: Types.ObjectId;
}

export interface IFullMessage extends IBaseMessage {
    _id: Types.ObjectId;
    likeCount: number;
    likers: Types.ObjectId[];
    dislikeCount: number;
    dislikers: Types.ObjectId[];
    author: {
        _id: Types.ObjectId;
        firstName: string;
        lastName?: string;
        email: string;
    }
}

export interface IPartialMessage {
    text?: string;
}
