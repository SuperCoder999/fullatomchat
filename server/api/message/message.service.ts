import {
    RAddMessage,
    RDeleteMessage,
    RGetAllMessages,
    RGetMessageById,
    RUpdateMessage,
    RLikeMessage,
    RDislikeMessage
} from "./message.repository";

import { IError, I_IDResult, I_OKResult } from "../types";
import { IMessage, IFullMessage, IPartialMessage } from "./message.types";
import { Types } from "mongoose";

export async function SAddMessage(data: IMessage): Promise<IError | I_IDResult> {
    try {
        const result: I_IDResult = await RAddMessage(data);
        return result;
    } catch (err) {
        return err;
    }
}

export async function SUpdateMessage(id: Types.ObjectId, data: IPartialMessage): Promise<IError | IFullMessage> {
    try {
        const result: IFullMessage = await RUpdateMessage(id, data);
        return result;
    } catch (err) {
        return err;
    }
}

export async function SGetAllMessages(): Promise<IFullMessage[] | IError> {
    try {
        const result: IFullMessage[] = await RGetAllMessages();
        return result;
    } catch (err) {
        return err;
    }
}

export async function SGetMessageById(id: Types.ObjectId): Promise<IError | IFullMessage> {
    try {
        const result: IFullMessage = await RGetMessageById(id);
        return result;
    } catch (err) {
        return err;
    }
}

export async function SDeleteMessage(id: Types.ObjectId): Promise<IError | I_OKResult> {
    try {
        const result: I_OKResult = await RDeleteMessage(id);
        return result;
    } catch (err) {
        return err;
    }
}

export async function SReactMessage(id: Types.ObjectId, userId: Types.ObjectId, isLike: boolean): Promise<IError | IFullMessage> {
    try {
        let result: IFullMessage;

        if (isLike) {
            result = await RLikeMessage(id, userId);
        } else {
            result = await RDislikeMessage(id, userId);
        }

        return result;
    } catch (err) {
        return err;
    }
}
