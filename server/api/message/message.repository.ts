import Message from "./message.model";
import { IMessage, IPartialMessage, IFullMessage } from "./message.types";
import { I_IDResult, I_OKResult } from "../types";
import { Types } from "mongoose";

export async function RAddMessage(data: IMessage): Promise<I_IDResult> {
    const { _id } = await Message.create(data);
    return { _id };
}

export async function RUpdateMessage(id: Types.ObjectId, data: IPartialMessage): Promise<IFullMessage> {
    const result: IFullMessage & { n?: number } = await Message
        .updateOne({ _id: id }, { ...data, editedAt: new Date() })
        .lean()
        .exec() as IFullMessage;

    if (result.n === 0) {
        throw {
            status: 404,
            message: "Message not found"
        }
    }

    return result;
}

export async function RGetAllMessages(): Promise<IFullMessage[]> {
    const result: IFullMessage[] = await Message
        .find({})
        .sort("-createdAt")
        .populate("author", "firstName lastName email")
        .lean()
        .exec() as IFullMessage[];

    return result;
}

export async function RGetMessageById(id: Types.ObjectId): Promise<IFullMessage> {
    const result: IFullMessage = await Message
        .findOne({ _id: id })
        .populate("author", "firstName lastName email")
        .lean()
        .exec() as IFullMessage;

    if (!result) {
        throw {
            status: 404,
            message: "Message not found"
        }
    }

    return result;
}

export async function RDeleteMessage(id: Types.ObjectId): Promise<I_OKResult> {
    const result: I_OKResult & { deletedCount?: number } | null = await Message.deleteOne({ _id: id }).lean().exec();
    
    if (!result || result.deletedCount === 0) {
        throw {
            status: 404,
            message: "Message not found"
        }
    }

    return result;
}

export async function RLikeMessage(id: Types.ObjectId, userId: Types.ObjectId): Promise<IFullMessage> {
    const message: IFullMessage = await RGetMessageById(id);
    const diff: number = message.likers.some((oid: Types.ObjectId) => oid.equals(userId)) ? -1 : 1;
    const index: number = message.likers.findIndex((oid: Types.ObjectId) => oid.equals(userId));
    const newLikers: Types.ObjectId[] = [...message.likers];

    if (diff > 0) {
        newLikers.push(userId);
    } else {
        newLikers.splice(index, 1);
    }

    const result: IFullMessage & { n?: number } = await Message
        .updateOne({ _id: id }, { likeCount: message.likeCount + diff, likers: newLikers })
        .lean()
        .exec() as IFullMessage;

    if (result.n === 0) {
        throw {
            status: 404,
            message: "Message not found"
        }
    }

    return result;
}

export async function RDislikeMessage(id: Types.ObjectId, userId: Types.ObjectId): Promise<IFullMessage> {
    const message: IFullMessage = await RGetMessageById(id);
    const diff: number = message.dislikers.some((oid: Types.ObjectId) => oid.equals(userId)) ? -1 : 1;
    const index: number = message.dislikers.findIndex((oid: Types.ObjectId) => oid.equals(userId));
    const newDislikers: Types.ObjectId[] = [...message.dislikers];

    if (diff > 0) {
        newDislikers.push(userId);
    } else {
        newDislikers.splice(index, 1);
    }

    const result: IFullMessage & { n?: number } = await Message
        .updateOne({ _id: id }, { dislikeCount: message.dislikeCount + diff, dislikers: newDislikers })
        .lean()
        .exec() as IFullMessage;

    if (result.n === 0) {
        throw {
            status: 404,
            message: "Message not found"
        }
    }

    return result;
}
