import { Types } from "mongoose";

export function oicast(id: string): Types.ObjectId {
    return Types.ObjectId(id);
}
