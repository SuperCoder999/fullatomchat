import React from "react";
import { Comment as Card, Icon } from "semantic-ui-react";
import { IMessage } from "../../containers/MessageList/types";
import moment from "moment";
import { State } from "../../redux/reduxTypes";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { deleteMessage, likeMessage, dislikeMessage } from "../../containers/MessageList/actions";
import { getUserName } from "../../containers/UsersList/helper";

interface MessageProps {
    message: IMessage;
    profileId: string | null;
    deleteMessage: (id: string) => void;
    likeMessage: (id: string) => void;
    dislikeMessage: (id: string) => void;
}

function Message({ message, profileId, deleteMessage, likeMessage, dislikeMessage }: MessageProps): JSX.Element {
    const isMine: boolean = message.author._id === profileId;

    return (
        <Card>
            <Card.Avatar />
            <Card.Content>
                <Card.Author>
                    {getUserName(message.author)}
                    {isMine ? " (Me)" : ""}
                </Card.Author>
                <Card.Metadata>
                    {moment(message.createdAt).calendar()}
                    {" "}
                    {message.editedAt
                        ? (
                            <span>(edited {moment(message.editedAt).fromNow()})</span>
                        )
                        : ""}
                </Card.Metadata>
                <Card.Text>{message.text}</Card.Text>
                <Card.Actions>
                    {isMine
                        ? (
                            <>
                                <Card.Action onClick={() => window.location.replace("/message/update?id=" + message._id)}>
                                    Edit
                                </Card.Action>
                                <Card.Action onClick={() => deleteMessage(message._id)}>
                                    Delete
                                </Card.Action>
                            </>
                        )
                        : (
                            <>
                                <Card.Action onClick={() => likeMessage(message._id)}>
                                    <Icon name="thumbs up outline" />
                                    {message.likeCount}
                                </Card.Action>
                                <Card.Action onClick={() => dislikeMessage(message._id)}>
                                    <Icon name="thumbs down outline" />
                                    {message.dislikeCount}
                                </Card.Action>
                            </>
                        )}
                </Card.Actions>
            </Card.Content>
        </Card>
    );
}

function mapStateToProps(state: State) {
    return {
        profileId: state.users.profile ? state.users.profile._id : null
    }
}

function mapDispatchToProps(dispatch: Dispatch) {
    return {
        deleteMessage: (id: string) => dispatch(deleteMessage(id)),
        likeMessage: (id: string) => dispatch(likeMessage(id)),
        dislikeMessage: (id: string) => dispatch(dislikeMessage(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Message);
