import React from "react";
import { Route, RouteProps } from "react-router";

interface PrivateRouteProps {
    isAuthorized: boolean;
    isAdmin?: boolean;
}

function PrivateRoute(props: PrivateRouteProps & RouteProps): JSX.Element | null {
    if (!props.isAuthorized) {
        window.location.replace("/login");
        return null;
    }

    if (props.isAdmin !== undefined ? !props.isAdmin : false) {
        window.location.replace("/");
        return null;
    }

    return <Route {...props} />
}

export default PrivateRoute;
