import React from "react";
import { Grid, Header, Statistic, Button } from "semantic-ui-react";
import { State } from "../../redux/reduxTypes";
import { connect } from "react-redux";
import { IMessage } from "../../containers/MessageList/types";
import moment from "moment";
import { Dispatch } from "redux";
import { logout } from "../../containers/UsersList/actions";
import { IUser } from "../../containers/UsersList/types";

interface SiteHeaderProps {
    messages: IMessage[];
    profile?: IUser;
    logout: () => void;
}

function SiteHeader({ messages, logout, profile }: SiteHeaderProps): JSX.Element {
    const users: string[] = messages.map((message: IMessage) => message.author._id);
    const usersSet = new Set<string>(users);
    const usersCount: number = usersSet.size;

    return (
        <Grid className="site-header fluid" textAlign="center" verticalAlign="middle" columns="3">
            <Grid.Column>
                <Header as="h1">Atom Chat</Header>
            </Grid.Column>
            <Grid.Column>
                <Statistic.Group size="small" style={{ justifyContent: "center" }}>
                    <Statistic>
                        <Statistic.Value>{usersCount}</Statistic.Value>
                        <Statistic.Label>Users posted</Statistic.Label>
                    </Statistic>
                    <Statistic>
                        <Statistic.Value>{messages.length}</Statistic.Value>
                        <Statistic.Label>
                            Messages
                            {messages[0] ? <span> - latest {moment(messages[0].createdAt).fromNow()}</span> : ""}
                        </Statistic.Label>
                    </Statistic>
                </Statistic.Group>
            </Grid.Column>
            <Grid.Column>
                <Button.Group>
                    <Button icon="sign out" title="Log out" negative onClick={() => logout()} className="inline" />
                    {profile
                        ? (
                            <>
                                {profile.isAdmin
                                    ? <Button positive onClick={() => window.location.replace("/admin")}>To admin page</Button>
                                    : ""}
                                <Button positive onClick={() => window.location.replace("/")}>To chat page</Button>
                            </>
                        ) : ""}
                </Button.Group>
            </Grid.Column>
        </Grid>
    );
}

function mapStateToProps(state: State) {
    return {
        messages: state.messages.messages,
        profile: state.users.profile
    }
}

function mapDispatchToProps(dispatch: Dispatch) {
    return {
        logout: () => dispatch(logout())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SiteHeader);
