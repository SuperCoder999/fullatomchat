import React from "react";
import { Dimmer, Loader } from "semantic-ui-react";

function Spinner(): JSX.Element {
    return (
        <Dimmer active>
            <Loader inverted size="massive" />
        </Dimmer>
    );
}

export default Spinner;
