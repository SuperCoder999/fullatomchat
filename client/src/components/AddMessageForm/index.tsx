import React, { useState } from "react";
import { addMessage } from "../../containers/MessageList/actions";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { Form, Button, TextAreaProps } from "semantic-ui-react";

interface AddMessageFormProps {
    addMessage: (text: string) => void;
}

function AddMessageForm(props: AddMessageFormProps): JSX.Element {
    const [text, setText] = useState<string>("");

    function submit() {
        props.addMessage(text);
        setText("");
    }

    return (
        <Form className="max-300 centered" onSubmit={submit}>
            <Form.TextArea
                placeholder="Message text"
                value={text}
                onChange={(event: React.FormEvent, data: TextAreaProps) => setText(data.value ? (data.value as string) : "")}
            />
            <Button fluid positive disabled={!text}>Add message</Button>
        </Form>
    );
}

function mapDispatchToProps(dispatch: Dispatch) {
    return {
        addMessage: (text: string) => dispatch(addMessage(text))
    }
}

export default connect(() => ({}), mapDispatchToProps)(AddMessageForm);
