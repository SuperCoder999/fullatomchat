import React from "react";
import SiteHeader from "../../components/SiteHeader";
import UsersList from "../UsersList";
import { Grid } from "semantic-ui-react";

function Admin(): JSX.Element | null {
    return (
        <>
            <SiteHeader />
            <Grid columns="1" className="fill" textAlign="center">
                <Grid.Column style={{ maxWidth: 700 }}>
                    <UsersList />
                </Grid.Column>
            </Grid>
        </>
    );
}

export default Admin;
