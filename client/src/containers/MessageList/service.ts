import { IMessage } from "./types";
import { API_URL } from "../../config";
import { I_IDResult } from "../../types";

export async function getMesssages(): Promise<IMessage[]> {
    const response: Response = await fetch(API_URL + "/messages");
    const json: IMessage[] = await response.json();
    return json;
}

export async function addMessage(text: string): Promise<I_IDResult> {
    const response: Response = await fetch(API_URL + "/messages", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ text })
    });

    const json: I_IDResult = await response.json();
    return json;
}

export async function updateMessage(id: string, text: string) {
    await fetch(API_URL + "/messages/" + id, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ text })
    });
}

async function reactMessage(id: string, isLike: boolean) {
    await fetch(API_URL + "/messages/react/" + id, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ isLike })
    });
}

export async function likeMessage(id: string) {
    await reactMessage(id, true);
}

export async function dislikeMessage(id: string) {
    await fetch(API_URL + "/messages/react/" + id, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ isLike: false })
    });
}

export async function deleteMessage(id: string) {
    await fetch(API_URL + "/messages/" + id, {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json"
        }
    });
}

export async function getMessageById(id: string): Promise<IMessage> {
    const response: Response = await fetch(API_URL + "/messages/" + id);
    const json: IMessage = await response.json();
    return json;
}
