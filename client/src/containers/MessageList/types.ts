export interface IMessage {
    _id: string;
    text: string;
    likeCount: number;
    likers: string[];
    dislikeCount: number;
    dislikers: string[];
    createdAt: Date;
    editedAt?: Date;
    author: {
        _id: string;
        firstName: string;
        lastName?: string;
        email: string;
    };
}
