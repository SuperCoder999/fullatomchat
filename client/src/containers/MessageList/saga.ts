import { takeEvery, put, all, call } from "redux-saga/effects";
import { ActionType, Action } from "../../redux/reduxTypes";
import { IMessage } from "./types";
import { getMesssages, addMessage, updateMessage, likeMessage, dislikeMessage, deleteMessage } from "./service";
import { loadMessagesSuccess, loadMessages } from "./actions";
import { showError } from "../../error.helper";

function* loadMessagesGen() {
    try {
        const result: IMessage[] = yield call(getMesssages);
        yield put(loadMessagesSuccess(result));
    } catch (err) {
        showError(err.message); //
    }
}

function* onLoadMessages() {
    yield takeEvery(ActionType.LoadMessages, loadMessagesGen);
}

function* addMessageGen({ data: { text } }: Action) {
    try {
        yield call(addMessage, text);
        yield put(loadMessages());
    } catch (err) {
        showError(err.message); //
    }
}

function* onAddMessage() {
    yield takeEvery(ActionType.AddMessage, addMessageGen);
}

function* updateMessageGen({ data: { id, text } }: Action) {
    try {
        yield call(updateMessage, id, text);
        window.location.replace("/");
    } catch (err) {
        showError(err.message); //
    }
}

function* onUpdateMessage() {
    yield takeEvery(ActionType.UpdateMessage, updateMessageGen);
}

function* likeMessageGen({ data: { id } }: Action) {
    try {
        yield call(likeMessage, id);
        yield put(loadMessages());
    } catch (err) {
        showError(err.message); //
    }
}

function* onLikeMessage() {
    yield takeEvery(ActionType.LikeMessage, likeMessageGen);
}

function* dislikeMessageGen({ data: { id } }: Action) {
    try {
        yield call(dislikeMessage, id);
        yield put(loadMessages());
    } catch (err) {
        showError(err.message); //
    }
}

function* onDislikeMessage() {
    yield takeEvery(ActionType.DislikeMessage, dislikeMessageGen);
}

function* deleteMessageGen({ data: { id } }: Action) {
    try {
        yield call(deleteMessage, id);
        yield put(loadMessages());
    } catch (err) {
        showError(err.message); //
    }
}

function* onDeleteMessage() {
    yield takeEvery(ActionType.DeleteMessage, deleteMessageGen);
}

export default function* () {
    yield all([
        onLoadMessages(),
        onAddMessage(),
        onUpdateMessage(),
        onLikeMessage(),
        onDislikeMessage(),
        onDeleteMessage()
    ]);
}
