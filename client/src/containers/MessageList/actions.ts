import { Action, ActionType } from "../../redux/reduxTypes";
import { IMessage } from "./types";

export function loadMessages(): Action {
    return {
        type: ActionType.LoadMessages
    }
}

export function loadMessagesSuccess(messages: IMessage[]): Action {
    return {
        type: ActionType.LoadMessagesSuccess,
        data: {
            messages
        }
    }
}

export function addMessage(text: string): Action {
    return {
        type: ActionType.AddMessage,
        data: {
            text
        }
    }
}

export function updateMessage(id: string, text: string): Action {
    return {
        type: ActionType.UpdateMessage,
        data: {
            id,
            text
        }
    }
}

export function likeMessage(id: string): Action {
    return {
        type: ActionType.LikeMessage,
        data: {
            id
        }
    }
}

export function dislikeMessage(id: string): Action {
    return {
        type: ActionType.DislikeMessage,
        data: {
            id
        }
    }
}

export function deleteMessage(id: string): Action {
    return {
        type: ActionType.DeleteMessage,
        data: {
            id
        }
    }
}
