import React from "react";
import { Comment as Card } from "semantic-ui-react";
import { connect } from "react-redux";
import { State } from "../../redux/reduxTypes";
import { IMessage } from "./types";
import Message from "../../components/Message";

interface MessageListProps {
    messages: IMessage[];
}

function MessageList({ messages }: MessageListProps): JSX.Element {
    return (
        <Card.Group style={{ marginTop: 20 }}>
            {messages.map((message: IMessage, index: number): JSX.Element => {
                return (
                    <Message key={index} message={message} />
                );
            })}
        </Card.Group>
    );
}

function mapStateToProps(state: State) {
    return {
        messages: state.messages.messages
    }
}

export default connect(mapStateToProps)(MessageList);
