import { MessageListState, Action, ActionType } from "../../redux/reduxTypes";

const initalState: MessageListState = {
    messages: []
};

export default (state: MessageListState = initalState, action: Action): MessageListState => {
    switch (action.type) {
        case ActionType.LoadMessagesSuccess: {
            return {
                ...state,
                messages: [...action.data.messages]
            };
        }
        default: {
            return state;
        }
    }
};
