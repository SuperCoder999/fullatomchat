import React from "react";
import SiteHeader from "../../components/SiteHeader";
import { Grid, Image } from "semantic-ui-react";
import logo from "../../images/logo.svg";
import MessageList from "../MessageList";
import AddMessageForm from "../../components/AddMessageForm";

class Chat extends React.Component {
    public render(): JSX.Element {
        return (
            <main className="fill">
                <SiteHeader />
                <Grid columns="2">
                    <Grid.Column>
                        <Image
                            title="Atom Chat"
                            alt="Atom Chat"
                            className="infinite-rotate"
                            src={logo}
                            centered
                            size="big"
                        />
                        <AddMessageForm />
                    </Grid.Column>
                    <Grid.Column>
                        <MessageList />
                    </Grid.Column>
                </Grid>
            </main>
        );
    }
}

export default Chat;
