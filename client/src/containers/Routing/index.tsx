import React from "react";
import { Route, Router, Switch } from "react-router";
import { createBrowserHistory, BrowserHistory } from "history";
import Chat from "../Chat";
import NotFound from "../../scenes/NotFound";
import { loadProfile, loadUsers } from "../UsersList/actions";
import { loadMessages } from "../MessageList/actions";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { IUser } from "../UsersList/types";
import { State } from "../../redux/reduxTypes";
import PrivateRoute from "../../components/PrivateRoute";
import LoginPage from "../LoginPage";
import Spinner from "../../components/Spinner";
import EditMessagePage from "../EditMessagePage";
import Admin from "../Admin";
import UserEditor from "../UserEditor";

interface RoutingProps {
    profile?: IUser;
    loadProfile: () => void;
    loadMessages: () => void;
    loadUsers: () => void;
    loaded: boolean;
}

interface RoutingState {
    loadedAllData: boolean;
}

class Routing extends React.Component<RoutingProps, RoutingState> {
    constructor(props: RoutingProps) {
        super(props);

        this.state = {
            loadedAllData: false
        }
    }

    public componentDidMount() {
        this.props.loadProfile();
    }

    public componentDidUpdate() {
        if (this.props.loaded && this.props.profile && !this.state.loadedAllData) {
            this.props.loadMessages();

            if (this.props.profile.isAdmin) {
                this.props.loadUsers();
            }

            this.setState({ loadedAllData: true });
        }
    }

    public render(): JSX.Element {
        const history: BrowserHistory = createBrowserHistory();

        if (!this.props.loaded) return <Spinner />

        const isAuthorized = Boolean(this.props.profile);
        const isAdmin = this.props.profile ? this.props.profile.isAdmin : false;

        return (
            <Router history={history}>
                <Switch>
                    <PrivateRoute path="/" isAuthorized={isAuthorized} exact component={Chat} />
                    <PrivateRoute path="/message/update" isAuthorized={isAuthorized} exact component={EditMessagePage} />
                    <PrivateRoute path="/admin" isAuthorized={isAuthorized} isAdmin={isAdmin} exact component={Admin} />
                    <PrivateRoute path="/user/manage" isAuthorized={isAuthorized} isAdmin={isAdmin} exact component={UserEditor} />
                    <Route path="/login" exact component={LoginPage} />
                    <Route path="*" exact component={NotFound} />
                </Switch>
            </Router>
        );
    }
}

function mapStateToProps(state: State) {
    return {
        profile: state.users.profile,
        loaded: state.users.profileLoaded
    }
}

function mapDispatchToProps(dispatch: Dispatch) {
    return {
        loadProfile: () => dispatch(loadProfile()),
        loadMessages: () => dispatch(loadMessages()),
        loadUsers: () => dispatch(loadUsers())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Routing);
