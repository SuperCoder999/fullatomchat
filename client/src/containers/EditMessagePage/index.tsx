import React from "react";
import { Form, Grid, Segment, Header, Button, TextAreaProps } from "semantic-ui-react";
import { getMessageById } from "../MessageList/service";
import { IMessage } from "../MessageList/types";
import { Dispatch } from "redux";
import { updateMessage } from "../MessageList/actions";
import { connect } from "react-redux";
import Spinner from "../../components/Spinner";
import { showError } from "../../error.helper";

interface EditMessagePageState {
    text: string;
    loaded: boolean;
}

interface EditMessagePageProps {
    updateMessage: (id: string, text: string) => void;
}

class EditMessagePage extends React.Component<EditMessagePageProps, EditMessagePageState> {
    public constructor(props: EditMessagePageProps) {
        super(props);

        this.state = {
            text: "",
            loaded: false
        }
    }

    public componentDidMount() {
        const params = new URLSearchParams(window.location.search);
        const id: string | null = params.get("id");

        if (!id) {
            window.location.replace("/");
            return;
        }

        getMessageById(id)
            .then((message: IMessage) => {
                this.setState({
                    loaded: true,
                    text: message.text
                })
            })
            .catch(err => showError(err.message)); //
    }

    public setMessage(text: string) {
        this.setState({
            text
        });
    }

    public submit() {
        const params = new URLSearchParams(window.location.search);
        const id: string = params.get("id") as string;
        this.props.updateMessage(id, this.state.text);
    }

    public render(): JSX.Element {
        if (!this.state.loaded) {
            return <Spinner />
        }

        return (
            <Grid className="fill" textAlign="center" verticalAlign="middle" columns="1">
                <Grid.Column style={{ maxWidth: 400 }}>
                    <Header as="h1" color="teal">Edit your message</Header>
                    <Segment>
                        <Form onSubmit={() => this.submit()}>
                            <Form.TextArea
                                placeholder="Message"
                                value={this.state.text}
                                onChange={(event: React.FormEvent, data: TextAreaProps) => {
                                    this.setMessage(data.value ? (data.value as string) : "");
                                }}
                            />
                            <Button fluid positive type="submit" disabled={!this.state.text}>Update</Button>
                        </Form>
                    </Segment>
                </Grid.Column>
            </Grid>
        );
    }
}

function mapDispatchToProps(dispatch: Dispatch) {
    return {
        updateMessage: (id: string, text: string) => dispatch(updateMessage(id, text))
    }
}

export default connect(() => ({}), mapDispatchToProps)(EditMessagePage);
