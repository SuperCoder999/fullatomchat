import { IUser, IBaseUser, IPrimaryUser } from "./types";
import { Action, ActionType } from "../../redux/reduxTypes";

export function loadUsersSuccess(users: IUser[]): Action {
    return {
        type: ActionType.LoadUsersSuccess,
        data: {
            users
        }
    };
}

export function loadUsers(): Action {
    return {
        type: ActionType.LoadUsers
    }
}

export function loadProfileSuccess(profile: IUser): Action {
    return {
        type: ActionType.LoadProfileSuccess,
        data: {
            profile
        }
    }
}

export function loadProfile(): Action {
    return {
        type: ActionType.LoadProfile
    }
}

export function addUser(settings: IBaseUser): Action {
    return {
        type: ActionType.AddUser,
        data: {
            ...settings
        }
    }
}

export function updateUser(id: string, settings: IPrimaryUser): Action {
    return {
        type: ActionType.UpdateUser,
        data: {
            id,
            data: {
                ...settings
            }
        }
    }
}

export function deleteUser(id: string): Action {
    return {
        type: ActionType.DeleteUser,
        data: {
            id
        }
    }
}

export function login(email: string, password: string): Action {
    return {
        type: ActionType.LogIn,
        data: {
            email,
            password
        }
    }
}

export function logout(): Action {
    return {
        type: ActionType.LogOut
    }
}
