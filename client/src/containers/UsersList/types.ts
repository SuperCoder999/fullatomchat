export interface IPrimaryUser {
    firstName: string;
    lastName?: string;
    email: string;
    isAdmin: boolean;
}

export interface IBaseUser extends IPrimaryUser {
    password: string;
}

export interface IUser extends IBaseUser {
    _id: string;
}
