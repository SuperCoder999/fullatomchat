export function getUserName(user: { firstName: string, lastName?: string }): string {
    return (user.firstName + " " + (user.lastName || "")).trim();
}
