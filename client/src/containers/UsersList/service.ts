import { IUser, IBaseUser, IPrimaryUser } from "./types";
import { API_URL } from "../../config";
import { I_IDResult } from "../../types";

export async function getAllUsers(): Promise<IUser[]> {
    const response: Response = await fetch(API_URL + "/users");
    const json: IUser[] = await response.json();
    return json;
}

export async function getProfile(): Promise<IUser | undefined> {
    const response: Response = await fetch(API_URL + "/users/profile");
    const json: IUser | { message?: string } = await response.json();
    return (json as { message?: string }).message ? undefined : (json as IUser);
}

export async function login(email: string, password: string): Promise<boolean> {
    const response: Response = await fetch(API_URL + "/users/login", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ email, password })
    });

    const json: { isAdmin: boolean } | { message?: string } = await response.json();
    if ((json as { message: string }).message) throw json;

    return (json as { isAdmin: boolean }).isAdmin;
}

export async function logout(): Promise<boolean> {
    const response: Response = await fetch(API_URL + "/users/logout", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        }
    });

    const json: { success: boolean } | { message?: string } = await response.json();
    if ((json as { message: string }).message) throw json;

    return (json as { success: boolean }).success;
}

export async function addUser(settings: IBaseUser): Promise<I_IDResult> {
    const response: Response = await fetch(API_URL + "/users", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(settings)
    });

    const json: I_IDResult | { message?: string } = await response.json();
    if ((json as { message?: string }).message) throw json;

    return json as I_IDResult;
}

export async function updateUser(id: string, settings: IPrimaryUser): Promise<I_IDResult> {
    const response: Response = await fetch(API_URL + "/users/" + id, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(settings)
    });

    const json: I_IDResult | { message?: string } = await response.json();
    if ((json as { message?: string }).message) throw json;

    return json as I_IDResult;
}

export async function deleteUser(id: string): Promise<void> {
    await fetch(API_URL + "/users/" + id, {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json"
        }
    });
}

export async function getUserById(id: string): Promise<IUser> {
    const response: Response = await fetch(API_URL + "/users/" + id);
    const json: IUser = await response.json();
    return json;
}
