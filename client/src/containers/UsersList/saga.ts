import { put, takeEvery, call, all } from "redux-saga/effects";
import { IUser } from "./types";
import { getAllUsers, getProfile, addUser, updateUser, deleteUser, login, logout } from "./service";
import { loadUsersSuccess, loadProfileSuccess, loadUsers } from "./actions";
import { ActionType, Action } from "../../redux/reduxTypes";
import { showError } from "../../error.helper";

function* loadUsersGen() {
    try {
        const users: IUser[] = yield call(getAllUsers);
        yield put(loadUsersSuccess(users));
    } catch (err) {
        showError(err.message); //
    }
}

function* onLoadUsers() {
    yield takeEvery(ActionType.LoadUsers, loadUsersGen);
}

function* loadProfileGen() {
    try {
        const profile: IUser = yield call(getProfile);
        yield put(loadProfileSuccess(profile));
    } catch (err) {
        //
    }
}

function* onLoadProfile() {
    yield takeEvery(ActionType.LoadProfile, loadProfileGen);
}

function* addUserGen({ data: { email, firstName, lastName, password, isAdmin } }: Action) {
    try {
        yield call(addUser, {
            email,
            firstName,
            lastName,
            password,
            isAdmin
        });

        window.location.replace("/admin");
    } catch (err) {
        showError(err.message); //
    }
}

function* onAddUser() {
    yield takeEvery(ActionType.AddUser, addUserGen);
}

function* updateUserGen({ data: { id, data: { email, firstName, lastName, isAdmin } } }: Action) {
    try {
        yield call(updateUser, id, {
            email,
            firstName,
            lastName,
            isAdmin
        });

        window.location.replace("/admin");
    } catch (err) {
        showError(err.message); //
    }
}

function* onUpdateUser() {
    yield takeEvery(ActionType.UpdateUser, updateUserGen);
}

function* deleteUserGen({ data: { id } }: Action) {
    try {
        yield call(deleteUser, id);
        yield put(loadUsers());
    } catch (err) {
        showError(err.message); //
    }
}

function* onDeleteUser() {
    yield takeEvery(ActionType.DeleteUser, deleteUserGen);
}

function* loginGen({ data: { email, password } }: Action) {
    try {
        const isAdmin: boolean = yield call(login, email, password);
        window.location.replace(isAdmin ? "/admin" : "/");
    } catch (err) {
        showError(err.message);
    }
}

function* onLogin() {
    yield takeEvery(ActionType.LogIn, loginGen);
}

function* logoutGen() {
    try {
        yield call(logout);
        window.location.reload();
    } catch (err) {
        showError(err.message);
    }
}

function* onLogout() {
    yield takeEvery(ActionType.LogOut, logoutGen);
}

export default function* () {
    yield all([
        onLoadUsers(),
        onLoadProfile(),
        onAddUser(),
        onUpdateUser(),
        onDeleteUser(),
        onLogin(),
        onLogout()
    ]);
}
