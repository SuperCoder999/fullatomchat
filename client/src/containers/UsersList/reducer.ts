import { UsersListState, Action, ActionType } from "../../redux/reduxTypes";

const initalState: UsersListState = {
    users: [],
    profileLoaded: false
}

export default (state: UsersListState = initalState, action: Action): UsersListState => {
    switch (action.type) {
        case ActionType.LoadUsersSuccess: {
            return {
                ...state,
                users: [...action.data.users]
            }
        }
        case ActionType.LoadProfileSuccess: {
            return {
                ...state,
                profile: action.data.profile,
                profileLoaded: true
            }
        }
        default: {
            return state;
        }
    }
};
