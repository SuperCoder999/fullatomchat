import React from "react";
import { Table, Button } from "semantic-ui-react";
import { connect } from "react-redux";
import { State } from "../../redux/reduxTypes";
import { IUser } from "./types";
import { getUserName } from "./helper";
import { Dispatch } from "redux";
import { deleteUser } from "./actions";

interface UsersListProps {
    users: IUser[];
    deleteUser: (id: string) => void;
}

function UsersList({ users, deleteUser }: UsersListProps): JSX.Element {
    return (
        <>
            <Button positive onClick={() => window.location.replace("/user/manage")} className="centered">Add user</Button>
            <Table celled className="centered">
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Email</Table.HeaderCell>
                        <Table.HeaderCell>Name</Table.HeaderCell>
                        <Table.HeaderCell>Is admin</Table.HeaderCell>
                        <Table.HeaderCell>Actions</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {users.map((user: IUser, i: number): JSX.Element => {
                        return (
                            <Table.Row key={i}>
                                <Table.Cell>{user.email}</Table.Cell>
                                <Table.Cell>{getUserName(user)}</Table.Cell>
                                <Table.Cell positive={user.isAdmin}>{user.isAdmin ? "Yes" : "No"}</Table.Cell>
                                <Table.Cell>
                                    <Button.Group>
                                        <Button positive onClick={() => window.location.replace("/user/manage?id=" + user._id)}>Edit</Button>
                                        <Button secondary inverted onClick={() => deleteUser(user._id)}>Delete</Button>
                                    </Button.Group>
                                </Table.Cell>
                            </Table.Row>
                        );
                    })}
                </Table.Body>
            </Table>
        </>
    );
}

function mapStateToProps(state: State) {
    return {
        users: state.users.users
    }
}

function mapDispatchToProps(dispatch: Dispatch) {
    return {
        deleteUser: (id: string) => dispatch(deleteUser(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersList);
