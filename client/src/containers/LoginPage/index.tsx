import React, { useState } from "react";
import { Grid, Form, Button, Header, Segment, InputProps } from "semantic-ui-react";
import validator from "validator";
import Spinner from "../../components/Spinner";
import { showError } from "../../error.helper";
import { Dispatch } from "redux";
import { login } from "../UsersList/actions";
import { connect } from "react-redux";

interface LoginPageProps {
    login: (email: string, password: string) => void;
}

function LoginPage({ login }: LoginPageProps) {
    const [email, Email] = useState<string>("");
    const [emailValid, setEmailValid] = useState<boolean>(true);
    const [password, Password] = useState<string>("");
    const [passwordValid, setPasswordValid] = useState<boolean>(true);
    const [loading, setLoading] = useState<boolean>(false);

    function setEmail(value: string): void {
        Email(value);
        setEmailValid(true);
    }

    function setPassword(value: string): void {
        Password(value);
        setPasswordValid(true);
    }

    async function submit(): Promise<void> {
        if (!emailValid || !passwordValid) return;

        try {
            setLoading(true);
            login(email, password);
        } catch (err) {
            showError(err.message); //
        }

        setLoading(false);
    }

    if (loading) {
        return <Spinner />
    }

    return (
        <Grid className="fill" columns="1" textAlign="center" verticalAlign="middle">
            <Grid.Column style={{ maxWidth: 400 }}>
                <Header as="h2" color="teal">Log in to your account</Header>
                <Segment>
                    <Form onSubmit={submit}>
                        <Form.Input
                            type="email"
                            icon="at"
                            iconPosition="left"
                            fluid
                            placeholder="Email"
                            onChange={(event: React.FormEvent, data: InputProps) => setEmail(data.value)}
                            onBlur={() => setEmailValid(validator.isEmail(email))}
                            error={!emailValid}
                        />
                        <Form.Input
                            type="password"
                            icon="lock"
                            iconPosition="left"
                            fluid
                            placeholder="Password"
                            onChange={(event: React.FormEvent, data: InputProps) => setPassword(data.value)}
                            onBlur={() => setPasswordValid(Boolean(password))}
                            error={!passwordValid}
                        />
                        <Button
                            type="submit"
                            positive
                            fluid
                            disabled={!(passwordValid && emailValid)}
                        >
                            Log in
                        </Button>
                    </Form>
                </Segment>
            </Grid.Column>
        </Grid>
    );
}

function mapDispatchToProps(dispatch: Dispatch) {
    return {
        login: (email: string, password: string) => dispatch(login(email, password))
    }
}

export default connect(() => ({}), mapDispatchToProps)(LoginPage);
