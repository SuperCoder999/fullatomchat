import React from "react";
import { Form, Grid, Button, Segment, InputProps, CheckboxProps } from "semantic-ui-react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { addUser, updateUser } from "../UsersList/actions";
import { IBaseUser, IUser, IPrimaryUser } from "../UsersList/types";
import validator from "validator";
import { getUserById } from "../UsersList/service";
import Spinner from "../../components/Spinner";
import { showError } from "../../error.helper";

interface UserEditorProps {
    addUser: (settings: IBaseUser) => void;
    updateUser: (id: string, settings: IBaseUser) => void;
}

interface UserEditorState extends IBaseUser {
    emailValid: boolean;
    firstNameValid: boolean;
    passwordValid: boolean;
    loading: boolean;
}

class UserEditor extends React.Component<UserEditorProps, UserEditorState> {
    protected id: string | null;

    constructor(props: UserEditorProps) {
        super(props);
        this.state = this.getBaseState();
        const params = new URLSearchParams(window.location.search);
        this.id = params.get("id");
    }

    public getBaseState(): UserEditorState {
        return {
            email: "",
            emailValid: true,
            firstName: "",
            firstNameValid: true,
            password: "",
            passwordValid: true,
            isAdmin: false,
            loading: false
        }
    }

    public componentDidMount() {
        if (this.id) {
            this.setState({ loading: true });

            getUserById(this.id)
                .then((user: IUser) => {
                    this.setState({ ...(user as IBaseUser), loading: false });
                })
                .catch(err => showError(err.message)); //
        }
    }

    public submit() {
        if (!this.id) {
            this.props.addUser(this.state);
        } else {
            this.props.updateUser(this.id, this.state);
        }
    }

    public render(): JSX.Element {
        if (this.state.loading) return <Spinner />;

        return (
            <Grid className="fill" columns="1" textAlign="center" verticalAlign="middle">
                <Grid.Column style={{ maxWidth: 400 }}>
                    <Segment>
                        <Form onSubmit={() => this.submit()}>
                            <Form.Input
                                type="email"
                                placeholder="Email"
                                fluid
                                icon="at"
                                iconPosition="left"
                                defaultValue={this.state.email}
                                onChange={(event: React.FormEvent, data: InputProps) => this.setState({
                                    email: data.value,
                                    emailValid: true
                                })}
                                onBlur={() => this.setState({ emailValid: validator.isEmail(this.state.email) })}
                                error={!this.state.emailValid}
                            />
                            <Form.Input
                                placeholder="First name"
                                fluid
                                icon="users"
                                iconPosition="left"
                                defaultValue={this.state.firstName}
                                onChange={(event: React.FormEvent, data: InputProps) => this.setState({
                                    firstName: data.value,
                                    firstNameValid: true
                                })}
                                onBlur={() => this.setState({ firstNameValid: Boolean(this.state.firstName) })}
                                error={!this.state.firstNameValid}
                            />
                            <Form.Input
                                placeholder="Last name"
                                fluid
                                icon="users"
                                iconPosition="left"
                                defaultValue={this.state.lastName || ""}
                                onChange={(event: React.FormEvent, data: InputProps) => this.setState({ lastName: data.value })}
                            />
                            {!this.id
                                ? (
                                    <Form.Input
                                        type="password"
                                        placeholder="Password"
                                        fluid
                                        icon="lock"
                                        iconPosition="left"
                                        defaultValue={this.state.password}
                                        onChange={(event: React.FormEvent, data: InputProps) => this.setState({
                                            password: data.value,
                                            passwordValid: true
                                        })}
                                        onBlur={() => this.setState({ emailValid: Boolean(this.state.password) })}
                                        error={!this.state.passwordValid}
                                    />
                                )
                                : ""}
                            <Form.Checkbox
                                toggle
                                defaultChecked={this.state.isAdmin}
                                label={<label>Is admin</label>}
                                onChange={(event: React.FormEvent, data: CheckboxProps) => this.setState({ isAdmin: data.checked || false })}
                            />
                            <Button positive fluid type="submit">Save user</Button>
                        </Form>
                    </Segment>
                </Grid.Column>
            </Grid>
        );
    }
}

function mapDispatchToProps(dispatch: Dispatch) {
    return {
        addUser: (settings: IBaseUser) => dispatch(addUser(settings)),
        updateUser: (id: string, settings: IPrimaryUser) => dispatch(updateUser(id, settings))
    }
}

export default connect(() => ({}), mapDispatchToProps)(UserEditor);
