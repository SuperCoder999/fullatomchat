export const IS_PRODUCTION = process.env.NODE_ENV === "production";
export const API_URL = IS_PRODUCTION ? "http://full-atom-chat.herokuapp.com/api" : "http://localhost:3001/api";
