import React from "react";
import { Button, Header, Grid } from "semantic-ui-react";

function NotFound(): JSX.Element {
    return (
        <Grid columns="1" className="fill black-background" textAlign="center" verticalAlign="middle">
            <Grid.Column style={{ maxWidth: 400 }}>
                <Header as="h1" color="red" inverted>Page not found</Header>
                <Button positive fluid compact onClick={() => window.history.back()}>Back</Button>
            </Grid.Column>
        </Grid>
    );
}

export default NotFound;
