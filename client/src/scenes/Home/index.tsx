import React from "react";
import Routing from "../../containers/Routing";
import { Provider } from "react-redux";
import store from "../../redux/store";
import { NotificationContainer } from "react-notifications";
import ReduxDevTools from "../../containers/ReduxDevTools";
import { IS_PRODUCTION } from "../../config";

function Home(): JSX.Element {
    return (
        <Provider store={store}>
            <NotificationContainer />
            <Routing />
            {!IS_PRODUCTION ? <ReduxDevTools /> : ""}
        </Provider>
    );
}

export default Home;
