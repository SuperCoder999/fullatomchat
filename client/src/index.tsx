import React from "react";
import ReactDOM from "react-dom";
import * as serviceWorker from "./serviceWorker";

import "./styles/reset.scss";
import "semantic-ui-css/semantic.min.css";
import "react-notifications/lib/notifications.css";
import "./styles/common.scss";

import Home from "./scenes/Home";

const request = window.fetch;

window.fetch = async (input: RequestInfo, init?: RequestInit | undefined): Promise<Response> => {
    return request(input, { ...init, credentials: "include" });
}

ReactDOM.render(
    <Home />,
    document.getElementById("root")
);

serviceWorker.register();
