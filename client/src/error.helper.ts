import { NotificationManager } from "react-notifications";

export function showError(message: string) {
    NotificationManager.error(message, "Error!", 3000);
}
