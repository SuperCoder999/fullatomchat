import { combineReducers } from "redux";
import messages from "../containers/MessageList/reducer";
import users from "../containers/UsersList/reducer";

export default combineReducers({ messages, users });
