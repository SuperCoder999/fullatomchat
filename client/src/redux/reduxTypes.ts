import { IMessage } from "../containers/MessageList/types";
import { IUser } from "../containers/UsersList/types";

export enum ActionType {
    LoadUsers = "loadUsers",
    LoadUsersSuccess = "loadUsersSuccess",
    LoadProfile = "loadProfile",
    LoadProfileSuccess = "loadProfileSuccess",
    LoadMessages = "loadMessages",
    LoadMessagesSuccess = "loadMessagesSuccess",
    AddMessage = "addMessage",
    UpdateMessage = "updateMessage",
    LikeMessage = "likeMessage",
    DislikeMessage = "dislikeMessage",
    DeleteMessage = "deleteMessage",
    AddUser = "addUser",
    UpdateUser = "updateUser",
    DeleteUser = "deleteUser",
    LogIn = "logIn",
    LogOut = "logOut"
}

export interface MessageListState {
    messages: IMessage[];
}

export interface UsersListState {
    users: IUser[];
    profile?: IUser;
    profileLoaded: boolean;
}

export interface State {
    messages: MessageListState;
    users: UsersListState;
}

export interface Action {
    type: ActionType;
    data?: any;
}
