import { all } from "redux-saga/effects";
import users from "../containers/UsersList/saga";
import messages from "../containers/MessageList/saga";

export default function* () {
    yield all([
        users(),
        messages()
    ]);
}
