import { createStore, Store, applyMiddleware, compose } from "redux";
import createSagaMiddleware, { SagaMiddleware } from "redux-saga";
import reducer from "./reducer";
import saga from "./saga";
import ReduxDevTools from "../containers/ReduxDevTools";
import { IS_PRODUCTION } from "../config";

const sagaMiddleware: SagaMiddleware = createSagaMiddleware();
const middleware = applyMiddleware(sagaMiddleware);

const enchancer = IS_PRODUCTION
    ? middleware
    : compose(middleware, ReduxDevTools.instrument())

const store: Store = createStore(
    reducer,
    enchancer
);

sagaMiddleware.run(saga);

export default store;
