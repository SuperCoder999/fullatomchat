# Atom Chat

## Install for production
 - cd server
 - npm i
 - npm run build
 - cd ../client
 - npm i
 - npm run build
 - cd ..
 - npm start

## Install for dev
 - cd server
 - npm i
 - npm start
 - cd ../client
 - npm i
 - npm start
 